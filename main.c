#include "constants.h"
#include <stdio.h>
#include <stdlib.h>

void initialize();
void update();
void draw();

Color* color;

u_long* data[3];
Sprite* sprites[3];

int gameMode = 0, alphaState = 0, kruncaniteCount = 0, timer = 0;
const int cooler = 30;

time_t t;

int main() {
    initialize();
    while(1) {
        update();
        clear_display();
        draw();
        display();
    }
}

void initialize() {
    initialize_heap();
    initialize_screen();
    initialize_pad();
    color_create(0, 0, 255, &color);
    set_background_color(color);
	
	cd_open();
	cd_read_file("LOGO.TIM", &data[0]);
	cd_read_file("SLIDE.VAG", &data[1]);
	cd_close();
	
	sprite_create((u_char *)data[0], 0, 0, &sprites[0]);
	
	audio_init();
	audio_transfer_vag_to_spu((u_char *)data[1], SECTOR *21, SPU_00CH);
	
	free3(data[1]);
	srand((unsigned) &t);
}

void update() {
	pad_update();
	if (gameMode == 1)
	{
		if (pad_check(Pad1Right))
		{
			sprites[1]->x += 5;	
		}
		else if (pad_check(Pad1Left))
		{
			sprites[1]->x -= 5;
		}
		
		if (alphaState == 0)
		{
			if (pad_check(Pad1Cross))
			{
				alphaState = 1;
				audio_play(SPU_00CH);
				if (sprites[1]->x < sprites[2]->x + sprites[2]->w &&
				sprites[1]->x + sprites[1]->w > sprites[2]->x)
				{
					audio_play(SPU_01CH);
					audio_play(SPU_02CH);
					kruncaniteCount++;
					sprites[2]->x = rand() % 200;
				}
			}
		}
		else
		{
			if (timer >= cooler)
			{
				alphaState = 0;
				timer = 0;
			}
			else
			{
				timer++;
			}
		}
		// if (sprites[0]->x > 200)
		// {
			// audio_play(SPU_00CH);
			// readNewData();
		// }
	}
	else if (pad_check(Pad1Cross) || pad_check(Pad1Start))
	{
		audio_play(SPU_00CH);
		loadGame();
	}
}

void loadGame()
{
	free3(data[0]);
	cd_open();
	// cd_read_file("HPUP.VAG", &data[0]);
	cd_read_file("COLLEGE.TIM", &data[0]);
	cd_read_file("ALPHAGUN.TIM", &data[1]);
	cd_read_file("KRN.TIM", &data[2]);
	cd_read_file("SHOTGUN.VAG", &data[3]);
	cd_read_file("BOOM.VAG", &data[4]);
	cd_read_file("SLIME.VAG", &data[5]);
	cd_close();
	
	sprite_create((u_char *)data[0], 0, 0, &sprites[0]);
	sprite_create((u_char *)data[1], 32, 170, &sprites[1]);
	sprite_create((u_char *)data[2], 100, 130, &sprites[2]);
	sprite_create((u_char *)data[2], 120, 130, &sprites[2]);
	
	audio_transfer_vag_to_spu((u_char *)data[3], SECTOR *21, SPU_00CH);
	audio_transfer_vag_to_spu((u_char *)data[4], SECTOR *21, SPU_01CH);
	audio_transfer_vag_to_spu((u_char *)data[5], SECTOR *21, SPU_02CH);
	
	free3(data[3]);
	free3(data[4]);
	free3(data[5]);
	
	gameMode = 1;
}

void draw() {
	if (gameMode == 1)
	{
		// draw_sprite(sprites[0]);
		draw_sprite(sprites[1]);
		draw_sprite(sprites[2]);
		//draw_sprite(sprites[2]);
	}
	else
	{
		draw_sprite(sprites[0]);
	}
}
